import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:navigation/ui/SecondScreen.dart';

class FirstScreen extends StatefulWidget {
  @override
  _FirstScreenState createState() {
    return _FirstScreenState();
  }
}
class _FirstScreenState extends State<FirstScreen> {
  String duration = 'Duration';
   var items = [];
  String currentDate = DateFormat.yMMMMEEEEd("en_US").format(DateTime.now()).toString();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('Sleep Tracker')),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                Icons.supervised_user_circle,
                size: 45,
                color: Colors.amber,
              ),
              Flexible(
                  child: new Text(
                    "Get to know your baby's sleep patterns and keep track of how much sleep they are getting here.",
                    textAlign: TextAlign.center,
                  )),
              FlatButton(
                color: Colors.blue,
                textColor: Colors.white,
                disabledColor: Colors.grey,
                disabledTextColor: Colors.black,
                padding: EdgeInsets.all(8.0),
                splashColor: Colors.blueAccent,
                onPressed: () {
                  _awaitReturnValueFromSecondScreen(context);
                },
                child: Text(
                  "Add New Sleeping Record",
                  style: TextStyle(fontSize: 20.0),
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(currentDate,
                      style: TextStyle(
                          fontSize: 25,
                          color: Colors.black54,
                          fontWeight: FontWeight.bold)),
                  Card(
                    color: Colors.teal.shade200,
                    margin: EdgeInsets.all(0),
                    elevation: 20,


                  child: ListView.separated(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: items.length,
                    itemBuilder: (context, i) {
                      return ListTile(
                        leading: Text(DateFormat.Hm("en_US").format(DateTime.now()).toString()),
                        title: Text(items[i].toString()),
                        subtitle: Text(items[i].toString()),
                      );
                    },
                    separatorBuilder: (context, i) {
                      return Divider();
                    },
                  ),
        )
                ],
              )
            ],
          ),
        ));
  }

  void _awaitReturnValueFromSecondScreen(BuildContext context) async {
    // start the SecondScreen and wait for it to finish with a result
    final result = await Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => SecondScreen(),
        ));

    // after the SecondScreen result comes back update
    setState(() {
    items.add(result);
    debugPrint(items.toString());
    
    });
  }


}

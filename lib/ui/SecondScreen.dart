import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:flutter_picker/flutter_picker.dart';
class SecondScreen extends StatefulWidget {
  @override
  _SecondScreenState createState() {
    return _SecondScreenState();
  }
}
class _SecondScreenState extends State<SecondScreen> {
  List duration;
  List sleepType = ["Nap"];
  String currentTime = DateFormat.yMMMMd("en_US").add_Hm().format(DateTime.now()).toString();

  showPickerNumber(BuildContext context) {
    new Picker(
        adapter: NumberPickerAdapter(data: [
          NumberPickerColumn(begin: 0, end: 24),
          NumberPickerColumn(begin: 0, end: 59),
        ]),
        delimiter: [
          PickerDelimiter(
              child: Container(
                width: 30.0,
                alignment: Alignment.center,
                child: Icon(Icons.more_vert),
              ))
        ],
        hideHeader: true,
        title: new Text("Please Select"),
        onConfirm: (Picker picker, List value) {
         duration = value;

          print(value.toString());
          print(picker.getSelectedValues());
setState(() {
  duration = value;
});
        }).showDialog(context);

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.white),
          title:
          Text("Sleeping Tracker", style: TextStyle(color: Colors.white))),
      body: Center(
        child: Column(children: <Widget>[
          Container(child: new Image.asset("assets/images/momandbaby.jpg")),

          ListTile(
            leading: Icon(Icons.calendar_today, color: Colors.blueAccent),
            title: Text("Date and Time",
                style: TextStyle(color: Colors.blueAccent)),
            subtitle: Text(currentTime),
          ),
          ListTile(
            leading: Icon(Icons.brightness_3, color: Colors.blueAccent),
            title:
            Text("Sleep Type", style: TextStyle(color: Colors.blueAccent)),
            subtitle: Text('Night, nap, etc'),
            onTap: () {},
          ),
          ListTile(
            leading: Icon(Icons.access_time, color: Colors.blueAccent),
            title: Text("Sleep Duration",
                style: TextStyle(color: Colors.blueAccent)),
            subtitle: Text(duration.toString()),

            onTap: () {
              showPickerNumber(context);
            },
          ),

          FlatButton(
            color: Colors.blue,
            textColor: Colors.white,
            disabledColor: Colors.grey,
            disabledTextColor: Colors.black,
            padding: EdgeInsets.only(left: 160, right: 160),
            splashColor: Colors.blueAccent,
            onPressed: () {
              _sendDataBack(context);
            },
            child: Text(
              "Save",
              style: TextStyle(fontSize: 15.0),
            ),
          )
        ]),
      ),
    );
  }

//send data to first screen
  void _sendDataBack(BuildContext context) {
    List sendData = new List.from(duration)..addAll(sleepType);
    Navigator.pop(context, sendData);
  }

}
